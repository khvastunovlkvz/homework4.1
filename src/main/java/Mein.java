public class Mein {

    public static void main(String[] args) {

        RectangleClass rectangle = new RectangleClass("Blue",10,10);    //Создан объект прямоугольник

        rectangle.moveUp(150);      //Сдвиг прямоугольника вверх на 150
        rectangle.moveLeft(3);      //Сдвиг прямоугольника влево на 3
        rectangle.getCoordinates(); //Проверка положения прямоугольника
        rectangle.getLength();      //Расчет периметра прямоугольника

        RoundClass round = new RoundClass("Green", 5);      //Создан объект круг

        round.moveDown(5);      //Сдвиг круга вниз на 5
        round.moveRight(3);     //Сдвиг круга вправо на 3
        round.getCoordinates(); //Проверка положения круга
    }
}
