public class RectangleClass extends Figure {

    public RectangleClass(){                                        //Пустой конструктор
        System.out.println();
        System.out.println("New Rectangle Created!");
        System.out.println();
    }

    public RectangleClass( int width, int height){                  //Конструктор с инициализацией ширины и высоты
        System.out.println();
        System.out.println("Объект создан");
        System.out.println("Ширина = " + width);
        System.out.println("Высота = " + height);
        System.out.println();
        this.height = height;
        this.width = width;
    }

    public RectangleClass(String color, int width, int height){     //Конструктор с инициализацией цвета, ширины и высоты
        super(color);
        System.out.println();
        System.out.println("Объект создан");
        System.out.println("Ширина = " + width);
        System.out.println("Высота = " + height);
        System.out.println("Цвет объекта : " + color);
        System.out.println();
        this.height = height;
        this.width = width;
    }

    int width;
    int height;

    public void getArea(){                                           //Метод расчет площади
        int area = width * height;
        System.out.println("Площадь равна : "+ area);
    }

    public void getLength(){     //Метод расчет периметра
        int perimeter = 2*(width + height);
        System.out.println("Периметр равен : " + perimeter);
    }

    public void sqare(){                                          //Метод является ли фигура квадратом или прямоугольником?
       if (width == height){
           System.out.println("Это квадрат! ");
       } else {
           System.out.println("Это прямоугольник! ");
       }

    }





}
