public class Figure {

    int x = 0;
    int y = 0;
    String color;

    public Figure(){                                             //Пустой конструктор
        System.out.println();
        System.out.println("Запуск конструктора Figure");
        System.out.println("Конструктор Figure завершен");
        System.out.println();
    }

    public  Figure(String color) {                              //Конструктор с инициализацией цвета
        this.color = color;
        System.out.println();
        System.out.println("Запуск конструктора Figure");
        System.out.println("Инициализация координат : x = " + x + "; y = " + y);
        System.out.println("Цвет : " + color);
        System.out.println("Конструктор Figure завершен");
        System.out.println();
    }

    public int moveLeft(int left){                              //Метод сдвиг влево
        if (left >= 0) {        //Запрет ввода отрицательных чисел
            x -= left;
            System.out.println("Фигура сдвинута влево на: " + left);
            return x;
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
        return x;
    }

    public int moveRight(int right){                            //Метод сдвиг вправо
        if(right >= 0) {        //Запрет ввода отрицательных чисел
            x += right;
            System.out.println("Фигура сдвинута вправо на: " + right);
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
        return x;

    }
    public int moveUp(int up){                                  //Метод сдвиг вверх
        if (up >= 0) {          //Запрет ввода отрицательных чисел
            y += up;
            System.out.println("Фигура сдвинута вверх на: " + up);
            return y;
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
        return y;
    }
    public int moveDown(int down){                              //Метод сдвиг вниз
        if (down >= 0) {        //Запрет ввода отрицательных чисел
            y -= down;
            System.out.println("Фигура сдвинута вверх на: " + down);
            return y;
        } else {
            System.out.println("Ошибка! Введенное значение меньше 0");
        }
        return y;
    }
    public void getCoordinates(){                               //Метод инициализация конечных координат
        System.out.println("Текущее положение фигуры по координатам : x = " + x + "; y = " + y);
    }




}