import org.testng.Assert;
import org.testng.annotations.Test;

public class TestFigure extends BaseTest {

    public RectangleClass rectangle = new RectangleClass("Blue", 10, 15);   //Создан объект прямоугольник
    /*//
    Модификатор доступа первоначально был private, при коммите IDEA ругнулась на это,
    типа rectangle является константой final. Так же произошло и с round. Почему?

     */
    public RoundClass round = new RoundClass("Blue", 10);       //Создан объект круг

    @Test       //Тест сдвига вправо
    public void checkMoveRight(){
        int expectedResult = (rectangle.x + 3);
        int actualResult = rectangle.moveRight(3);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test       //Тест сдвига влево
    public void checkMoveLeft(){
        int expectedResult = (rectangle.x - 3);
        int actualResult = rectangle.moveLeft(3);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test       //Тест сдвига вверх
    public void checkMoveUp(){
        int expectedResult = (round.y + 3);
        int actualResult = round.moveUp(3);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test       //Тест сдвига вниз
    public void checkMoveDown(){
        int expectedResult = (round.y - 3);
        int actualResult = round.moveDown(3);

        Assert.assertEquals(expectedResult, actualResult);
    }





}
