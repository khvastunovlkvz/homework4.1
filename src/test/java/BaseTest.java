import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

    @BeforeTest
    public void beforeTest(){
        System.out.println();
        System.out.println("Старт метода перед тестом");
    }

    @AfterTest
    public void afterTest(){
        System.out.println();
        System.out.println("Старт метода после теста");
    }
}
